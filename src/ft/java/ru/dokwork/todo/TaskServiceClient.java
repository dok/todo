package ru.dokwork.todo;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.UUID;

/**
 * Client of {@link ru.dokwork.todo.TaskService}
 */
public class TaskServiceClient {

    private final String URL;
    private final JsonParser parser;

    public HttpResponse getTask(UUID uuid) throws IOException {
        String newUrl = URL + uuid.toString();
        return Request.Get(newUrl).execute().returnResponse();
    }

    public HttpResponse getAll() throws IOException {
        return Request.Get(URL)
                .execute().returnResponse();
    }

    public HttpResponse addNewTask(JsonObject jtask) throws IOException {
        return Request.Post(URL)
                .bodyString(jtask.toString(), ContentType.APPLICATION_JSON)
                .execute().returnResponse();
    }

    public HttpResponse putTask(UUID uuid, JsonObject jtask) throws IOException {
        return Request.Put(URL + uuid)
                .bodyString(jtask.toString(), ContentType.APPLICATION_JSON)
                .execute().returnResponse();
    }

    public HttpResponse find(boolean completed) throws IOException {
        String newUrl = URL + "find?completed=" + completed;
        return Request.Get(newUrl).execute().returnResponse();
    }

    public HttpResponse delete(UUID uuid) throws IOException {
        return Request.Delete(URL + uuid.toString())
                .execute()
                .returnResponse();
    }

    public HttpResponse patch(UUID uuid, JsonObject patch) throws URISyntaxException, IOException {
        URIBuilder uri = new URIBuilder().setPath(URL + uuid);
        HttpPatch method = new HttpPatch(uri.build());
        HttpEntity httpEntity = new StringEntity(patch.toString(), MediaType.APPLICATION_JSON, "UTF-8");
        method.setEntity(httpEntity);
        HttpClient httpclient = HttpClients.createDefault();
        return httpclient.execute(method);
    }

    public int getTasksCount() throws IOException {
        String json = readResponseToString(getAll());
        if (json.isEmpty()) {
            return 0;
        }
        return parser.parse(json).getAsJsonArray().size();
    }

    public void removeAllExistingTasks() throws IOException {
        HttpResponse httpResponse = getAll();
        String json = readResponseToString(httpResponse);
        if (json.isEmpty()) {
            return;
        }
        JsonArray array = parser.parse(json).getAsJsonArray();
        for (JsonElement jsonElement : array) {
            JsonObject jtask = jsonElement.getAsJsonObject();
            delete(UUID.fromString(jtask.get("uuid").getAsString()));
        }
    }

    public TaskServiceClient(String url) {
        URL = url;
        parser = new JsonParser();
    }

    private static String readResponseToString(HttpResponse response) throws IOException {
        HttpEntity entity = response.getEntity();
        if (entity == null) {
            return "";
        }
        InputStream content = entity.getContent();
        java.util.Scanner s = new java.util.Scanner(content).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}

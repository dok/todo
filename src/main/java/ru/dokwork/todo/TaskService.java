package ru.dokwork.todo;

import ru.dokwork.todo.dao.MockTaskDao;
import ru.dokwork.todo.dao.TaskDao;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;


@Path("/tasks")
@Produces({"application/json"})
public class TaskService {

    private TaskDao dao;

    /**
     * Get task by uuid.
     *
     * @param uuid identifier of task
     * @return task with specific uuid.
     */
    @GET
    @Path("/{uuid}")
    public Task get(@PathParam("uuid") UUID uuid) {
        return dao.getByUUID(uuid);
    }

    /**
     * Add new task.
     *
     * @param task new task.
     */
    @POST
    @Path("/")
    @Consumes({"application/json"})
    public Response add(Task task) {
        dao.save(task);
        URI location = URI.create("/tasks/" + task.getUUID().toString());
        return Response.created(location).build();
    }

    /**
     * Update task with specific identifier
     *
     * @param uuid identifier of task, that be updated.
     * @param task task with new properties.
     * @return http response.
     */
    @PUT
    @Path("/{uuid}")
    @Consumes({"application/json"})
    public Response update(@PathParam("uuid") UUID uuid, Task task) {
        Task originTask = dao.getByUUID(uuid);
        boolean isNewTask = originTask == null;
        if (isNewTask) {
            originTask = new Task();
            originTask.setUUID(uuid);
        }
        originTask.setName(task.getName());
        originTask.setDescription(task.getDescription());
        originTask.setCompleted(task.isCompleted());
        dao.save(originTask);
        URI location = URI.create("/tasks/" + task.getUUID());
        if (isNewTask) {
            return Response.created(location).build();
        } else {
            return Response.noContent().location(location).build();
        }
    }

    /**
     * Modify properties of the task with specific identifier.
     *
     * @param uuid  identifier of task, that be edited.
     * @param patch object with the new values ​​of properties.
     */
    @PATCH
    @Path("/{uuid}")
    @Consumes({"application/json"})
    public Response patch(@PathParam("uuid") UUID uuid, Task patch) {
        Task originTask = dao.getByUUID(uuid);
        if (originTask == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        if (patch.getName() != null) {
            originTask.setName(patch.getName());
        }
        if (patch.getDescription() != null) {
            originTask.setDescription(patch.getDescription());
        }
        if (patch.isCompleted() != null) {
            originTask.setCompleted(patch.isCompleted());
        }
        dao.save(originTask);
        URI location = URI.create("/tasks/" + uuid);
        return Response.noContent().location(location).build();
    }

    /**
     * Delete task with specific identifier.
     *
     * @param uuid identifier of task, that be removed.
     * @return http response.
     */
    @DELETE
    @Path("/{uuid}")
    public Response delete(@PathParam("uuid") UUID uuid) {
        Task originTask = dao.getByUUID(uuid);
        if (originTask == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        dao.remove(originTask);
        return Response.noContent().build();
    }

    /**
     * Find all completed or uncompleted tasks.
     *
     * @param isCompleted mark of completed state for search.
     * @return collection with completed (if argument is true) or uncompleted (if argument is false) tasks.
     */
    @GET
    @Path("/find")
    public Collection<Task> find(@QueryParam("completed") Boolean isCompleted) {
        if (isCompleted == null) {
            return null;
        }
        return dao.findByStatus(isCompleted);
    }

    /**
     * Get list of all tasks.
     *
     * @return list of tasks.
     */
    @GET
    public Collection<Task> getAll() {
        return dao.getAll();
    }

    public TaskService() {
        dao = new MockTaskDao();
    }
}

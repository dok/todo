package ru.dokwork.todo.dao;

import ru.dokwork.todo.Task;

import java.util.Collection;
import java.util.UUID;

public interface TaskDao {

    /**
     * Get task by uuid.
     *
     * @param uuid identifier of task
     * @return task with specific uuid.
     */
    Task getByUUID(UUID uuid);

    /**
     * Save task.
     */
    void save(Task task);

    /**
     * Remove task.
     */
    void remove(Task task);

    /**
     * Find task with specific state.
     *
     * @param isCompleted state of needed tasks.
     * @return collection of tasks with specific state or empty collection.
     */
    Collection<Task> findByStatus(boolean isCompleted);

    /**
     * Get all tasks.
     *
     * @return collection of all tasks.
     */
    Collection<Task> getAll();
}
